package data.dao;

import data.entities.Classroom;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassroomDAO extends  AbstractDAO<Classroom> {

    public boolean delete(int id){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psClassroom = connection.prepareStatement("DELETE FROM classrooms WHERE id = ?;");

            psClassroom.setInt(1,id);

            int rowsAffected = psClassroom.executeUpdate();

            return rowsAffected > 0;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean createOrUpdate(Classroom classroom){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psClassrooms;

            if(classroom.getId() > 0) {

                psClassrooms = connection.prepareStatement("UPDATE classrooms SET classroom = ? WHERE id  = ?");

                psClassrooms.setString(1, classroom.getClassroom());
                psClassrooms.setInt(2, classroom.getId());
            }else{

                psClassrooms = connection.prepareStatement("INSERT INTO classrooms VALUES(?,?)");

                psClassrooms.setInt(1, classroom.getId());
                psClassrooms.setString(2, classroom.getClassroom());
            }

            int rowsAffected = psClassrooms.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }


    @Override
    protected List<Classroom> toEntity(ResultSet resultSet) throws SQLException {

        Classroom classroom;

        List<Classroom> classroomList = new ArrayList<>();

        while(resultSet.next()){

            classroom = new Classroom();

            classroom.setId(resultSet.getInt("id"));
            classroom.setClassroom(resultSet.getString("classroom"));

            classroomList.add(classroom);

        }

        return classroomList;

    }

    @Override
    protected String getQuery(Map<String, String[]> parameterMap) {

        String query = "SELECT * FROM classrooms";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()){

            query+=" WHERE";


            if(parameterMap.containsKey("id")){
                query+=" id = " + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("classroom")){
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" classroom = \'" + parameterMap.get("classroom")[0] + "\'";
            }


        }

        query+=';';

        return query;

    }

}
