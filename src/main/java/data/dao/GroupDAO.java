package data.dao;

import data.entities.Group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GroupDAO extends AbstractDAO<Group> {

    public boolean delete(int id){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psGroups = connection.prepareStatement("DELETE FROM `groups` WHERE id = ?;");

            psGroups.setInt(1,id);

            int rowsAffected = psGroups.executeUpdate();

            return rowsAffected > 0;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean createOrUpdate(Group group){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psGroups;

            if(group.getId() > 0) {

                psGroups = connection.prepareStatement("UPDATE `groups` SET group_name = ?, shift = ? WHERE id = ?");

                psGroups.setString(1, group.getGroupName());
                psGroups.setInt(2,group.getShift());
                psGroups.setInt(3, group.getId());
            }else{

                psGroups = connection.prepareStatement("INSERT INTO `groups` VALUES(?,?,?)");

                psGroups.setInt(1, group.getId());
                psGroups.setString(2, group.getGroupName());
                psGroups.setInt(3,group.getShift());
            }

            int rowsAffected = psGroups.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }


    protected String getQuery(Map<String, String[]> parameterMap){

        String query = "SELECT * FROM `groups`";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()){

            query+=" WHERE";


            if(parameterMap.containsKey("id")){
                query+=" id = " + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("name")){
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" group_name = \'" + parameterMap.get("name")[0] + "\'";
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("shift")){
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" shift = " + parameterMap.get("shift")[0];
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("class_id")){
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" id IN (SELECT group_id FROM group_class WHERE class_id = " + parameterMap.get("class_id")[0] + ")";
            }

        }

        query+=';';

        return query;

    }

    protected List<Group> toEntity(ResultSet rsSchedule) throws SQLException {

        Group group;

        List<Group> groupList = new ArrayList<>();

        while(rsSchedule.next()){

            group = new Group();

            group.setId(rsSchedule.getInt("id"));
            group.setGroupName(rsSchedule.getString("group_name"));
            group.setShift(rsSchedule.getInt("shift"));

            groupList.add(group);

        }

        return groupList;

    }

}
