package data.dao;

import data.entities.ClassForGroup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassForGroupDAO extends AbstractDAO<ClassForGroup> {

    public boolean delete(int group_id, int class_id){

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM group_class WHERE group_id = ? AND class_id = ?");

            preparedStatement.setInt(1,group_id);
            preparedStatement.setInt(2,class_id);

            int rowsAffected = preparedStatement.executeUpdate();

            return rowsAffected > 0;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean create(ClassForGroup[] cfgArray){

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            connection.setAutoCommit(false);
            connection.setSavepoint();

            for (ClassForGroup classForGroup:cfgArray) {

                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO group_class VALUES(?,?)");

                preparedStatement.setInt(1,classForGroup.getGroupId());
                preparedStatement.setInt(2,classForGroup.getClassId());

                int rowsAffected = preparedStatement.executeUpdate();

                if(rowsAffected == 0) throw new Exception("Weren't able to add one or more records. Rolling back changes...");
            }

            connection.commit();
            return true;

        }catch (Exception e){
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }finally {
            if(connection!=null){
                try {
                    connection.setAutoCommit(true);
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    protected List<ClassForGroup> toEntity(ResultSet resultSet) throws SQLException {

        List<ClassForGroup> classForGroupList = new ArrayList<>();

        while(resultSet.next()){

            classForGroupList.add(new ClassForGroup(resultSet.getInt("group_id"), resultSet.getInt("class_id")));

        }

        return classForGroupList;

    }

    //Now it doesn't take any parameters(at least it doesn't use them), but chances are it will in the future
    //So I consider this approach quite reasonable here
    @Override
    protected String getQuery(Map<String, String[]> parameterMap) {
        return "SELECT * FROM group_class";
    }
}
