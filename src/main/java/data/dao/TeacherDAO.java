package data.dao;

import data.entities.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TeacherDAO extends AbstractDAO<Teacher> {


    public boolean delete(int id){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psTeachers = connection.prepareStatement("DELETE FROM teachers WHERE id = ?;");

            psTeachers.setInt(1,id);

            int rowsAffected = psTeachers.executeUpdate();

            return rowsAffected > 0;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }


    public boolean createOrUpdate(Teacher teacher){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psTeachers;

            if(teacher.getId() > 0) {

                psTeachers = connection.prepareStatement("UPDATE teachers SET full_name = ? WHERE id  = ?");

                psTeachers.setString(1, teacher.getFullName());
                psTeachers.setInt(2, teacher.getId());
            }else{

                psTeachers = connection.prepareStatement("INSERT INTO teachers VALUES(?,?)");

                psTeachers.setInt(1, teacher.getId());
                psTeachers.setString(2, teacher.getFullName());
            }

            int rowsAffected = psTeachers.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }


    @Override
    protected String getQuery(Map<String,String[]> parameterMap) {

            String query = "SELECT * FROM teachers";
            boolean isMoreThanOneCondition = false;

            if(!parameterMap.isEmpty()){

                query+=" WHERE";


                if(parameterMap.containsKey("id")){
                    query+=" id = " + parameterMap.get("id")[0];
                    isMoreThanOneCondition = true;
                }
                if(parameterMap.containsKey("full_name")){
                    query+=(isMoreThanOneCondition)?" AND":"";
                    query+=" full_name = \'" + parameterMap.get("full_name")[0] + "\'";
                }


            }

            query+=';';

            return query;

        }

    protected List<Teacher> toEntity(ResultSet rsSchedule) throws SQLException {

        Teacher teacher;

        List<Teacher> teacherList = new ArrayList<>();

        while(rsSchedule.next()){

            teacher = new Teacher();

            teacher.setId(rsSchedule.getInt("id"));
            teacher.setFullName(rsSchedule.getString("full_name"));

            teacherList.add(teacher);

        }

        return teacherList;

    }

    }

