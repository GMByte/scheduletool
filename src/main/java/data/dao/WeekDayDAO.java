package data.dao;

import data.entities.WeekDay;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WeekDayDAO extends AbstractDAO<WeekDay> {

    public boolean delete(int id){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psWeekDays = connection.prepareStatement("DELETE FROM week_days WHERE id = ?;");

            psWeekDays.setInt(1,id);

            int rowsAffected = psWeekDays.executeUpdate();

            return rowsAffected > 0;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean createOrUpdate(WeekDay weekDay){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psWeekDays;

            if(weekDay.getId() > 0) {

                psWeekDays = connection.prepareStatement("UPDATE week_days SET day_name = ? WHERE id  = ?");

                psWeekDays.setString(1, weekDay.getDayName());
                psWeekDays.setInt(2, weekDay.getId());
            }else{

                psWeekDays = connection.prepareStatement("INSERT INTO week_days VALUES(?,?)");

                psWeekDays.setInt(1, weekDay.getId());
                psWeekDays.setString(2, weekDay.getDayName());
            }

            int rowsAffected = psWeekDays.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }

    protected String getQuery(Map<String, String[]> parameterMap){

        String query = "SELECT * FROM week_days";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()){

            query+=" WHERE";


            if(parameterMap.containsKey("id")){
                query+=" id = " + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("name")){
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" day_name = \'" + parameterMap.get("name")[0] + "\'";
            }


        }

        query+=';';

        return query;

    }

    protected List<WeekDay> toEntity(ResultSet rsSchedule) throws SQLException {

        WeekDay weekDay;

        List<WeekDay> weekDayList = new ArrayList<>();

        while(rsSchedule.next()){

            weekDay = new WeekDay();

            weekDay.setId(rsSchedule.getInt("id"));
            weekDay.setDayName(rsSchedule.getString("day_name"));

            weekDayList.add(weekDay);

        }

        return weekDayList;

    }

}
