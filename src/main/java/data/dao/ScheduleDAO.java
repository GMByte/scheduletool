package data.dao;

import data.entities.*;

import java.sql.*;
import java.util.*;


public class ScheduleDAO extends AbstractDAO<Schedule> {

    public static Map<String, Integer> idMap;

    public boolean delete(Map<String, String[]> parameterMap) {

        Connection connection = null;

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            String params = (parameterMap.containsKey("group_id")) ? " WHERE group_id = " + parameterMap.get("group_id")[0] : "";

            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM schedule" + params);

            int rowsAffected = preparedStatement.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }

    public boolean updateTable(Schedule[] schedules) {

        Connection connection = null;

        try {

            if (idMap != null) idMap.clear();

            connection = DBConnectionManager.getInstance().getConnection();

            connection.setAutoCommit(false);
            connection.setSavepoint();

            PreparedStatement preparedStatement;

            idMap = new HashMap<>();

            for (Schedule schedule : schedules) {

                ResultSet resultSet = connection.prepareStatement("SELECT id FROM week_days WHERE day_name = '" + schedule.getWeekDay() + "';").executeQuery();
                resultSet.next();
                idMap.put(schedule.getWeekDay(), resultSet.getInt("id"));

                resultSet = connection.prepareStatement("SELECT id FROM `groups` WHERE group_name = '" + schedule.getGroup() + "';").executeQuery();
                resultSet.next();
                idMap.put(schedule.getGroup(), resultSet.getInt("id"));

                resultSet = connection.prepareStatement("SELECT id FROM classes WHERE class_name = '" + schedule.getClassName() + "';").executeQuery();
                resultSet.next();
                idMap.put(schedule.getClassName(), resultSet.getInt("id"));


                resultSet = connection.prepareStatement("SELECT id FROM teachers WHERE full_name = \'" + schedule.getTeacherFullName() + "';").executeQuery();
                resultSet.next();
                idMap.put(schedule.getTeacherFullName(), resultSet.getInt("id"));


                resultSet = connection.prepareStatement("SELECT id FROM `time` WHERE `range` = \'" + schedule.getTime() + "';").executeQuery();
                resultSet.next();
                idMap.put(schedule.getTime(), resultSet.getInt("id"));

                resultSet = connection.prepareStatement("SELECT id FROM classrooms WHERE classroom = '" + schedule.getClassroom() + "';").executeQuery();
                resultSet.next();
                idMap.put(String.valueOf(schedule.getClassroom()), resultSet.getInt("id"));


                if (schedule.isToDelete()) {

                    preparedStatement = connection.prepareStatement("DELETE FROM schedule WHERE week_day_id = ? AND group_id = ? AND `number` = ?;");

                    preparedStatement.setInt(1, idMap.get(schedule.getWeekDay()));
                    preparedStatement.setInt(2, idMap.get(schedule.getGroup()));
                    preparedStatement.setInt(3, schedule.getNumber());

                    int rowsAffected = preparedStatement.executeUpdate();


                    if (rowsAffected == 0) {
                        connection.rollback();
                        return false;
                    }

                } else {

                    preparedStatement = connection.prepareStatement("SELECT week_day_id FROM schedule WHERE week_day_id = ? AND group_id = ? AND `number` = ?;");

                    preparedStatement.setInt(1, idMap.get(schedule.getWeekDay()));
                    preparedStatement.setInt(2, idMap.get(schedule.getGroup()));
                    preparedStatement.setInt(3, schedule.getNumber());

                    if (preparedStatement.executeQuery().next()) {

                        preparedStatement = connection.prepareStatement("UPDATE schedule SET class_id = ?, teacher_id = ?, time_id = ?, classroom_id = ?, changed = ?" +
                                " WHERE week_day_id = ? AND group_id = ? AND `number` = ?;");

                        preparedStatement.setInt(1, idMap.get(schedule.getClassName()));
                        preparedStatement.setInt(2, idMap.get(schedule.getTeacherFullName()));
                        preparedStatement.setInt(3, idMap.get(schedule.getTime()));
                        preparedStatement.setInt(4, idMap.get(String.valueOf(schedule.getClassroom())));
                        preparedStatement.setBoolean(5, schedule.isChanged());
                        preparedStatement.setInt(6, idMap.get(schedule.getWeekDay()));
                        preparedStatement.setInt(7, idMap.get(schedule.getGroup()));
                        preparedStatement.setInt(8, schedule.getNumber());

                    } else {

                        preparedStatement = connection.prepareStatement("INSERT INTO schedule VALUES (?,?,?,?,?,?,?,?);");

                        preparedStatement.setInt(1, idMap.get(schedule.getWeekDay()));
                        preparedStatement.setInt(2, idMap.get(schedule.getGroup()));
                        preparedStatement.setInt(3, idMap.get(schedule.getClassName()));
                        preparedStatement.setInt(4, idMap.get(schedule.getTeacherFullName()));
                        preparedStatement.setInt(5, schedule.getNumber());
                        preparedStatement.setInt(6, idMap.get(schedule.getTime()));
                        preparedStatement.setInt(7, idMap.get(String.valueOf(schedule.getClassroom())));
                        preparedStatement.setBoolean(8, schedule.isChanged());

                    }

                    int rowsAffected = preparedStatement.executeUpdate();


                    if (rowsAffected == 0) {
                        connection.rollback();
                        return false;
                    }
                }
            }

            connection.commit();
            connection.setAutoCommit(true);

            return true;

        } catch (Exception e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
            return false;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    protected String getQuery(Map<String, String[]> parameterMap) {

        String query = "SELECT wd.day_name, g.group_name, t.full_name, c.class_name, cr.classroom, s.number, g.shift, tm.range, s.changed FROM schedule s"
                + " LEFT JOIN week_days wd ON s.week_day_id = wd.id"
                + " LEFT JOIN `groups` g ON s.group_id = g.id"
                + " LEFT JOIN teachers t ON s.teacher_id = t.id"
                + " LEFT JOIN classes c ON s.class_id = c.id"
                + " LEFT JOIN time tm ON s.time_id = tm.id"
                + " LEFT JOIN classrooms cr ON s.classroom_id = cr.id";

        boolean isMoreThanOneCondition = false;

        if (!parameterMap.isEmpty()) {

            query += " WHERE";

            if (parameterMap.containsKey("group_id")) {
                query += " g.id = " + parameterMap.get("group_id")[0];
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("group_name")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " g.group_name = \'" + parameterMap.get("group_name")[0] + "\'";
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("day_id")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " wd.id = " + parameterMap.get("day_id")[0];
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("day_name")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " wd.day_name = \'" + parameterMap.get("day_name")[0] + "\'";
            }
            if (parameterMap.containsKey("teacher_id")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " t.id = " + parameterMap.get("teacher_id")[0];
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("teacher_name")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " t.full_name = \'" + parameterMap.get("teacher_name")[0] + "\'";
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("shift")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " group_id IN (SELECT id FROM `groups` WHERE shift = " + parameterMap.get("shift")[0] + ")";
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("number")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " s.number = " + parameterMap.get("number")[0];
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("changed")) {
                query += (isMoreThanOneCondition) ? " AND" : "";
                query += " s.changed = " + parameterMap.get("changed")[0];
            }

        }

        query += " ORDER BY week_day_id ASC, number ASC;";

        return query;

    }

    @Override
    protected List<Schedule> toEntity(ResultSet resultSet) throws SQLException {

        List<Schedule> scheduleList = new ArrayList();

        Schedule schedule;

        while (resultSet.next()) {

            schedule = new Schedule();

            schedule.setClassName(resultSet.getString("class_name"));
            schedule.setClassroom(resultSet.getString("classroom"));
            schedule.setTime(resultSet.getString("range"));
            schedule.setGroup(resultSet.getString("group_name"));
            schedule.setTeacherFullName(resultSet.getString("full_name"));
            schedule.setWeekDay(resultSet.getString("day_name"));
            schedule.setNumber(resultSet.getInt("number"));
            schedule.setShift(resultSet.getInt("shift"));
            schedule.setChanged(resultSet.getBoolean("changed"));

            scheduleList.add(schedule);

        }

        return scheduleList;

    }

}
