package data.dao;

import data.entities.ClassEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassDAO extends AbstractDAO<ClassEntity> {

    public boolean delete(int id){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psClass = connection.prepareStatement("DELETE FROM classes WHERE id = ?;");

            psClass.setInt(1,id);

            int rowsAffected = psClass.executeUpdate();

            return rowsAffected > 0;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean createOrUpdate(ClassEntity classEntity){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psClasses;

            if(classEntity.getId() > 0) {

                psClasses = connection.prepareStatement("UPDATE classes SET class_name = ? WHERE id  = ?");

                psClasses.setString(1, classEntity.getClassName());
                psClasses.setInt(2, classEntity.getId());
            }else{

                psClasses = connection.prepareStatement("INSERT INTO classes VALUES(?,?)");

                psClasses.setInt(1, classEntity.getId());
                psClasses.setString(2, classEntity.getClassName());
            }

            int rowsAffected = psClasses.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }

    @Override
    protected String getQuery(Map<String, String[]> parameterMap) {

        String query = "SELECT * FROM classes";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()){

            query+=" WHERE";


            if(parameterMap.containsKey("id")){
                query+=" id = " + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("name")){
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" class_name = \'" + parameterMap.get("name")[0] + "\'";
            }
            if(parameterMap.containsKey("group_id")){
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" id IN (SELECT class_id FROM group_class WHERE group_id = " + parameterMap.get("group_id")[0] + ")";
            }


        }

        query+=';';

        return query;

    }

    @Override
    protected List<ClassEntity> toEntity(ResultSet resultSet) throws SQLException {

        ClassEntity classEntity;

        List<ClassEntity> classList = new ArrayList<>();

        while(resultSet.next()){

            classEntity = new ClassEntity();

            classEntity.setId(resultSet.getInt("id"));
            classEntity.setClassName(resultSet.getString("class_name"));

            classList.add(classEntity);

        }
        return classList;
    }

}
