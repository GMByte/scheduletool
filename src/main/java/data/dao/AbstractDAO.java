package data.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public abstract class AbstractDAO<Entity> {

    Connection connection;

    protected abstract List<Entity> toEntity(ResultSet resultSet) throws SQLException;

    protected abstract String getQuery(Map<String, String[]> parameterMap);

    public List<Entity> findByParams(Map<String, String[]> parameterMap) {

        try {
            connection = DBConnectionManager.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Entity> entityList = null;

        try {
            ResultSet rsEntity = connection.prepareStatement(getQuery(parameterMap)).executeQuery();

            entityList = toEntity(rsEntity);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return entityList;

    }
}
