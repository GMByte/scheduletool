package data.dao;

import data.entities.Time;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TimeDAO extends AbstractDAO<Time>{

    public boolean delete(int id){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psTime = connection.prepareStatement("DELETE FROM `time` WHERE id = ?;");

            psTime.setInt(1,id);

            int rowsAffected = psTime.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean createOrUpdate(Time time){

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psTime;

            if (time.getId() > 0) {

                psTime = connection.prepareStatement("UPDATE `time` SET `range` = ? WHERE id  = ?");

                psTime.setString(1, time.getRange());
                psTime.setInt(2, time.getId());
            } else {

                psTime = connection.prepareStatement("INSERT INTO `time` VALUES(?,?)");

                psTime.setInt(1, time.getId());
                psTime.setString(2, time.getRange());
            }

            int rowsAffected = psTime.executeUpdate();

            return rowsAffected > 0;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }


    @Override
    protected String getQuery(Map<String, String[]> parameterMap) {

        String query = "SELECT * FROM time";
        boolean isMoreThanOneCondition = false;

        if (!parameterMap.isEmpty()){

            query+=" WHERE";

            if (parameterMap.containsKey("id")) {
                query+=" id = " + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("range")) {
                query+=(isMoreThanOneCondition)?" AND":"";
                query+=" range = \'" + parameterMap.get("range")[0] + "\'";
            }

        }

        query+=';';

        return query;

    }

    @Override
    protected List<Time> toEntity(ResultSet resultSet) throws SQLException {

        Time time;

        List<Time> timeList = new ArrayList<>();

        while (resultSet.next()){

            time = new Time();

            time.setId(resultSet.getInt("id"));
            time.setRange(resultSet.getString("range"));

            timeList.add(time);

        }

        return timeList;

    }


}
