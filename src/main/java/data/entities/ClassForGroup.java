package data.entities;

public class ClassForGroup {

    private int group_id, class_id;

    public ClassForGroup() {}

    public ClassForGroup(int group_id, int class_id) {
        this.group_id = group_id;
        this.class_id = class_id;
    }

    public int getGroupId() {
        return group_id;
    }

    public void setGroupId(int group_id) {
        this.group_id = group_id;
    }

    public int getClassId() {
        return class_id;
    }

    public void setClassId(int class_id) {
        this.class_id = class_id;
    }

    @Override
    public String toString() {
        return "ClassesForGroups{" +
                "group_id=" + group_id +
                ", class_id=" + class_id +
                '}';
    }
}
