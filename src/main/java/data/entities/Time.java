package data.entities;

public class Time {

    int id;
    String range;

    public Time() {}

    public Time(int id, String range) {
        this.id = id;
        this.range = range;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    @Override
    public String toString() {
        return "Time{" +
                "id=" + id +
                ", range='" + range + '\'' +
                '}';
    }
}
