package data.entities;

public class Classroom {

    private int id;
    private String classroom;

    public Classroom() {}

    public Classroom(int id, String classroom) {
        this.id = id;
        this.classroom = classroom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    @Override
    public String toString() {
        return "Classroom{" +
                "id=" + id +
                ", classroom=" + classroom +
                '}';
    }
}
