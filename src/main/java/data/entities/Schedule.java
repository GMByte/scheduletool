package data.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Schedule {

    private boolean toDelete;

    @JsonProperty("week_day")
    private String weekDay;

    private String group;

    @JsonProperty("class_name")
    private String className;

    @JsonProperty("teacher_full_name")
    private String teacherFullName;

    private int number, shift;
    private String time;
    private String classroom;
    private boolean changed;

    public Schedule() {}

    public Schedule(boolean toDelete, String weekDay, String group, String className, String teacherFullName, int number, int shift, String time, String classroom, boolean changed) {
        this.toDelete = toDelete;
        this.weekDay = weekDay;
        this.group = group;
        this.className = className;
        this.teacherFullName = teacherFullName;
        this.number = number;
        this.shift = shift;
        this.time = time;
        this.classroom = classroom;
        this.changed = changed;
    }

    public boolean isToDelete() { return toDelete; }

    public void setToDelete(boolean toDelete) { this.toDelete = toDelete; }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacherFullName() {
        return teacherFullName;
    }

    public void setTeacherFullName(String teacherFullName) {
        this.teacherFullName = teacherFullName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public int getShift() {
        return shift;
    }

    public void setShift(int shift) {
        this.shift = shift;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "toDelete=" + toDelete +
                ", weekDay='" + weekDay + '\'' +
                ", group='" + group + '\'' +
                ", className='" + className + '\'' +
                ", teacherFullName='" + teacherFullName + '\'' +
                ", number=" + number +
                ", shift=" + shift +
                ", time='" + time + '\'' +
                ", classroom='" + classroom + '\'' +
                ", changed=" + changed +
                '}';
    }
}
