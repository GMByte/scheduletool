package data.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class WeekDay {

    int id;

    @JsonProperty("day_name")
    String dayName;

    public WeekDay() {}

    public WeekDay(int id, String dayName) {
        this.id = id;
        this.dayName = dayName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    @Override
    public String toString() {
        return "WeekDay{" +
                "id=" + id +
                ", dayName='" + dayName + '\'' +
                '}';
    }
}
