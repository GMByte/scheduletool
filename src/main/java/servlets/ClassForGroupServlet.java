package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.ClassForGroupDAO;
import data.entities.ClassForGroup;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "ClassForGroupServlet")
public class ClassForGroupServlet extends AbstractServlet {

    private ClassForGroupDAO classForGroupDAO = new ClassForGroupDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {

        ClassForGroup[] resArray;

        try {
            resArray = new ObjectMapper().readValue(extractPostRequestBody(request), ClassForGroup[].class);
        } catch (IOException e) {
            return false;
        }

        return classForGroupDAO.create(resArray);

    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return classForGroupDAO.delete(Integer.parseInt(request.getParameter("group_id")),
                Integer.parseInt(request.getParameter("class_id")));
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return classForGroupDAO.findByParams(request.getParameterMap());
    }

}
