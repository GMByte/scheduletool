package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.GroupDAO;
import data.entities.Group;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "GroupServlet")
public class GroupServlet extends AbstractServlet {

    private GroupDAO groupDAO = new GroupDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {
        Group resGroup;

        try {
            resGroup = new ObjectMapper().readValue(extractPostRequestBody(request), Group.class);
        } catch (IOException e) {
            return false;
        }

        return groupDAO.createOrUpdate(resGroup);
    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return groupDAO.delete(Integer.parseInt(request.getParameter("id")));
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return groupDAO.findByParams(request.getParameterMap());
    }

}
