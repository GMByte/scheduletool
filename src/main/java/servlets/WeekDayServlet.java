package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.WeekDayDAO;
import data.entities.WeekDay;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "WeekDayServlet")
public class WeekDayServlet extends AbstractServlet {

    private WeekDayDAO weekDayDAO = new WeekDayDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {
        WeekDay resWeekDay;

        try {
            resWeekDay = new ObjectMapper().readValue(extractPostRequestBody(request), WeekDay.class);
        } catch (IOException e) {
            return false;
        }

        return weekDayDAO.createOrUpdate(resWeekDay);
    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return weekDayDAO.delete(Integer.parseInt(request.getParameter("id")));
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return weekDayDAO.findByParams(request.getParameterMap());
    }

}
