package servlets;

import firebase.ClientNotifier;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.ScheduleDAO;
import data.entities.Schedule;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "ScheduleServlet")
public class ScheduleServlet extends AbstractServlet {

    private ScheduleDAO scheduleDAO = new ScheduleDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {

        Schedule[] resArray;

        try {
            resArray = new ObjectMapper().readValue(extractPostRequestBody(request), Schedule[].class);
        } catch (IOException exception) { return false; }

        boolean operationSuccessful = scheduleDAO.updateTable(resArray);

        if (operationSuccessful) ClientNotifier.getInstance().notifyDevices(resArray);

        return operationSuccessful;

    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return scheduleDAO.delete(request.getParameterMap());
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return scheduleDAO.findByParams(request.getParameterMap());
    }

}
