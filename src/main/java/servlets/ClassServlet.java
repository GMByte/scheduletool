package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.ClassDAO;
import data.entities.ClassEntity;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "ClassServlet")
public class ClassServlet extends AbstractServlet {

    private ClassDAO classDAO = new ClassDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {
        ClassEntity resClass;

        try {
            resClass = new ObjectMapper().readValue(extractPostRequestBody(request), ClassEntity.class);
        } catch (IOException e) {
            return false;
        }

        return classDAO.createOrUpdate(resClass);
    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return classDAO.delete(Integer.parseInt(request.getParameter("id")));
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return classDAO.findByParams(request.getParameterMap());
    }

}
