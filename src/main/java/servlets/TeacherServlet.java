package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.TeacherDAO;
import data.entities.Teacher;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "TeacherServlet")
public class TeacherServlet extends AbstractServlet {

    private TeacherDAO teacherDAO = new TeacherDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {
        Teacher resTeacher;

        try {
            resTeacher = new ObjectMapper().readValue(extractPostRequestBody(request), Teacher.class);
        } catch (IOException e) {
            return false;
        }

        return new TeacherDAO().createOrUpdate(resTeacher);
    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return teacherDAO.delete(Integer.parseInt(request.getParameter("id")));
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return teacherDAO.findByParams(request.getParameterMap());
    }

}
