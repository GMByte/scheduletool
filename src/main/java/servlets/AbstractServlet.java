package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.entities.Result;
import utils.enums.OperationType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Scanner;

public abstract class AbstractServlet extends HttpServlet {

    private PostReqExecutor postReqExecutor = new PostReqExecutor();

    protected abstract boolean postReq(HttpServletRequest request);

    protected abstract boolean deleteReq(HttpServletRequest request);

    protected abstract Object getReq(HttpServletRequest request);

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset=utf8");

        Result result = postReqExecutor.execute(req, OperationType.DELETE, () -> deleteReq(req));

        resp.getWriter().append(new ObjectMapper().writeValueAsString(result));

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        Result result = postReqExecutor.execute(request, OperationType.UPDATE, () -> postReq(request));

        response.getWriter().append(new ObjectMapper().writeValueAsString(result));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        Object result = getReq(request);

        response.getWriter().append(new ObjectMapper().writeValueAsString(result));

    }

    protected String extractPostRequestBody(HttpServletRequest request) throws IOException {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            Scanner s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }

}
