package servlets;

import data.entities.Result;
import security.ModAccessValidator;
import utils.ResponseBuilder;
import utils.enums.OperationType;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;

public class PostReqExecutor {

    private ResponseBuilder responseBuilder = new ResponseBuilder();

    public interface Executor {
        boolean execute() throws AccessDeniedException;
    }

    public Result execute(HttpServletRequest request, OperationType operationType, Executor executor) {

        Result responseResult;

        try {

            ModAccessValidator.validateSecretWord(request.getHeader("password"));

            boolean result = executor.execute();

            if (result) responseResult = responseBuilder.buildSuccess(operationType);
            else responseResult = responseBuilder.buildFailure(operationType);

        } catch (AccessDeniedException exception){
            responseResult = responseBuilder.buildAccessViolate(operationType);
        }

        return responseResult;

    }

}
