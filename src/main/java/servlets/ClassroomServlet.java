package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.ClassroomDAO;
import data.entities.Classroom;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "ClassroomServlet")
public class ClassroomServlet extends AbstractServlet {

    private ClassroomDAO classroomDAO = new ClassroomDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {
        Classroom resClassroom;

        try {
            resClassroom = new ObjectMapper().readValue(extractPostRequestBody(request), Classroom.class);
        } catch (IOException e) {
            return false;
        }

        return classroomDAO.createOrUpdate(resClassroom);
    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return classroomDAO.delete(Integer.parseInt(request.getParameter("id")));
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return classroomDAO.findByParams(request.getParameterMap());
    }

}
