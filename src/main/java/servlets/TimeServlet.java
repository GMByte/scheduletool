package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.dao.TimeDAO;
import data.entities.Time;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebServlet(name = "TimeServlet")
public class TimeServlet extends AbstractServlet {

    private TimeDAO timeDAO = new TimeDAO();

    @Override
    protected boolean postReq(HttpServletRequest request) {
        Time resTime;

        try {
            resTime = new ObjectMapper().readValue(extractPostRequestBody(request), Time.class);
        } catch (IOException e) {
            return false;
        }

        return timeDAO.createOrUpdate(resTime);
    }

    @Override
    protected boolean deleteReq(HttpServletRequest request) {
        return timeDAO.delete(Integer.parseInt(request.getParameter("id")));
    }

    @Override
    protected Object getReq(HttpServletRequest request) {
        return timeDAO.findByParams(request.getParameterMap());
    }

}
