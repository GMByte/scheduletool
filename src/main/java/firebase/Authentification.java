package firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Authentification {

    private static FileInputStream serviceAccount = null;

    public static void initializeApplication(){
        if(serviceAccount==null) {
            try {
                serviceAccount = new FileInputStream("scheduletoolbipFirebase.json");

                FirebaseOptions options = new FirebaseOptions.Builder()
                        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                        .setDatabaseUrl("https://scheduletoolbip.firebaseio.com")
                        .build();

                FirebaseApp.initializeApp(options);
            } catch (FileNotFoundException e) {
                System.err.println("Initialization failed due to the absence of the firebase key");
                e.printStackTrace();
            } catch (IOException e) {
                System.err.println("Unable to initialize the application");
            }
        }
    }

}
