package firebase;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import data.entities.Schedule;

import java.util.*;

import static data.dao.ScheduleDAO.idMap;

public class ClientNotifier {

    private static ClientNotifier clientNotifier;

    private ClientNotifier(){}

    public static ClientNotifier getInstance() {
        if(clientNotifier == null) clientNotifier = new ClientNotifier();
        return clientNotifier;
    }

    public void notifyDevices(Schedule[] changeList){

        String title = "Ваше расписание изменилось!";

        Authentification.initializeApplication();

        List<String> notifiedList = new ArrayList<>();

        Map<String, Integer> changedDaysMap = getClosestChangedDaysMap(changeList);

        for(Schedule schedule:changeList){

            String groupTopic = "", teacherTopic = "";

            if(!notifiedList.contains("group" + idMap.get(schedule.getGroup())))
                groupTopic = "group" + idMap.get(schedule.getGroup());


            if(!notifiedList.contains("teacher" + idMap.get(schedule.getTeacherFullName())))
                teacherTopic = "teacher" + idMap.get(schedule.getTeacherFullName());


            if(!groupTopic.isEmpty()) {
                Message groupMessage = Message.builder()
                        .putData("title", title)
                        .putData("body", "Перейдите в приложение, чтобы увидеть изменения для группы " + schedule.getGroup())
                        .putData("weekDay", String.valueOf(changedDaysMap.get(groupTopic)))
                        .putData("for", "group")
                        .setTopic(groupTopic)
                        .build();

                try {
                    FirebaseMessaging.getInstance().send(groupMessage);
                } catch (FirebaseMessagingException e) {
                    e.printStackTrace();
                }

                if(!notifiedList.contains("group" + idMap.get(schedule.getGroup()))) notifiedList.add("group" + idMap.get(schedule.getGroup()));

            }

            if(!teacherTopic.isEmpty()) {
                Message teacherMessage = Message.builder()
                        .putData("title", title)
                        .putData("body", "Перейдите в приложение, чтобы увидеть изменения для преподавателя " + schedule.getTeacherFullName())
                        .putData("weekDay", String.valueOf(changedDaysMap.get(teacherTopic)))
                        .putData("for", "teacher")
                        .setTopic(teacherTopic)
                        .build();

                try {
                    FirebaseMessaging.getInstance().send(teacherMessage);
                } catch (FirebaseMessagingException e) {
                    e.printStackTrace();
                }

                if(!notifiedList.contains("teacher" + idMap.get(schedule.getTeacherFullName()))) notifiedList.add("teacher" + idMap.get(schedule.getTeacherFullName()));

            }

        }

    }

    private Map<String, Integer> getClosestChangedDaysMap(Schedule[] changeList) {

        Map<String, Integer> closestChangedDays = new HashMap<>();

        int todayID = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        todayID = (todayID != 1) ? todayID - 1 : 7;

        int processedDay;
        String targetKeyGroup, targetKeyTeacher;

        //Здесь в карту записываются близжайшие изменённые дни на эту неделю
        for (int i = 0; i < changeList.length; i++) {

            processedDay = idMap.get(changeList[i].getWeekDay());

            targetKeyGroup = "group" + idMap.get(changeList[i].getGroup());
            targetKeyTeacher = "teacher" + idMap.get(changeList[i].getTeacherFullName());

            if((!closestChangedDays.containsKey(targetKeyGroup) && processedDay >= todayID) || (processedDay >= todayID && processedDay < closestChangedDays.get(targetKeyGroup)))
                closestChangedDays.put(targetKeyGroup, processedDay);

            if((!closestChangedDays.containsKey(targetKeyTeacher) && processedDay >= todayID) || (processedDay >= todayID && processedDay < closestChangedDays.get(targetKeyTeacher)))
                closestChangedDays.put(targetKeyTeacher, processedDay);

        }

        //Здесь записываются в карту близжайшие изменённые дни на следующую неделю
        for (int i = 0; i < changeList.length; i++) {

            processedDay = idMap.get(changeList[i].getWeekDay());

            targetKeyGroup = "group" + idMap.get(changeList[i].getGroup());
            targetKeyTeacher = "teacher" + idMap.get(changeList[i].getTeacherFullName());

            if(!closestChangedDays.containsKey(targetKeyGroup) || processedDay < closestChangedDays.get(targetKeyGroup))
                closestChangedDays.put(targetKeyGroup, processedDay);

            if(!closestChangedDays.containsKey(targetKeyTeacher) || (processedDay > todayID && processedDay < closestChangedDays.get(targetKeyTeacher)))
                closestChangedDays.put(targetKeyTeacher, processedDay);

        }

        return closestChangedDays;
    }

}
