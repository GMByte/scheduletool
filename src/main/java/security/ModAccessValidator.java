package security;

import data.PropertyReader;

import java.nio.file.AccessDeniedException;

public class ModAccessValidator {

    public static void validateSecretWord(String enteredPassword) throws AccessDeniedException {

        String password = PropertyReader.getInstance().getProperty("secretWord");

        if(enteredPassword!=null) {
            if (enteredPassword.equals(password)) return;
        }

        throw new AccessDeniedException("AccessDenied");

    }

}
