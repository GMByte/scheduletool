package utils;

import data.entities.Result;
import utils.enums.OperationResult;
import utils.enums.OperationType;

public class ResponseBuilder {

    private static String ACCESS_DENIED_MESSAGE = "Access denied";

    public Result buildSuccess(OperationType operationType){

        Result result = new Result();

        result.setCode(OperationResult.OPERATION_SUCCESSFUL.getCode());

        if(operationType == OperationType.DELETE) {
            result.setMessage(OperationType.DELETE.getSuccessMsg());
        } else {
            result.setMessage(OperationType.UPDATE.getSuccessMsg());
        }

        return result;

    }

    public Result buildAccessViolate(OperationType operationType){

        Result result = new Result();

        result.setCode(OperationResult.ACCESS_DENIED.getCode());
        result.setMessage(ACCESS_DENIED_MESSAGE);

        return result;

    }

    public Result buildFailure(OperationType operationType){

        Result result = new Result();

        result.setCode(OperationResult.OPERATION_FAILED.getCode());

        if(operationType == OperationType.DELETE) {
            result.setMessage(OperationType.DELETE.getFailureMsg());
        } else {
            result.setMessage(OperationType.UPDATE.getFailureMsg());
        }

        return result;

    }

}
