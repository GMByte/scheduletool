package utils.enums;

public enum OperationResult {

    OPERATION_SUCCESSFUL(1),
    OPERATION_FAILED(2),
    ACCESS_DENIED(3);

    private int code;

    public int getCode() {
        return code;
    }

    OperationResult(int code) {
        this.code = code;
    }

}
