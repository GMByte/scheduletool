package utils.enums;

public enum OperationType {

    UPDATE("Successfully saved!", "Unable to save"),
    DELETE("Successfully deleted!", "Unable to delete");

    private String successMsg;
    private String failureMsg;

    public String getSuccessMsg() {
        return successMsg;
    }

    public String getFailureMsg() {
        return failureMsg;
    }

    OperationType(String successMsg, String failureMsg) {
        this.successMsg = successMsg;
        this.failureMsg = failureMsg;
    }

}
